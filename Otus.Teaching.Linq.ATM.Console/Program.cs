﻿using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //  1 задание

            var userLogin = "snow";
            var userPassword = "111";

            var userInfo = atmManager.ShowUserInfo(userLogin, userPassword);

            System.Console.WriteLine(@"Данные по логину ""{0}"" представлены ниже:", userLogin);

            System.Console.WriteLine($"Номер ID аккаунта : {userInfo.Id}\n" +
                                     $"Имя : {userInfo.FirstName}\n" + 
                                     $"Фамилия : {userInfo.SurName}\n" +
                                     $"Отчество : {userInfo.MiddleName}\n" +
                                     $"Телефон : {userInfo.Phone}\n" +
                                     $"Паспортные данные : {userInfo.PassportSeriesAndNumber}\n" +
                                     $"Дата регистрации : {userInfo.RegistrationDate}\n" +
                                     $"Логин : {userInfo.Login}\n");
            System.Console.WriteLine();

            //  2 задание

            var accountInfo = atmManager.ShowAccountInfo(userInfo);
            System.Console.WriteLine(@"Данные по по счетам пользователя ""{0}"" с логином ""{1}"" представлены ниже:", userInfo.FirstName, userInfo.Login);

            //  3 задание
            
            System.Console.WriteLine(@"Данные по счетам и истории пользователя ""{0}"" с логином ""{1}"" представлены ниже:", userInfo.FirstName, userInfo.Login);

            atmManager.ShowAccountHistoryInfo(userInfo);
            System.Console.WriteLine();
            
            // 4 задание

            System.Console.WriteLine("Данные о всех операциях пополнения счета:");

            atmManager.ShowInputCashOperations();
            System.Console.WriteLine();
            // 5 задание

            var checkSum = 10499;
            System.Console.WriteLine(@"Данные по о всех пользователях с суммой выше {0}:", checkSum);

            atmManager.ShowCashMoreThanN(checkSum);

            System.Console.WriteLine("Завершение работы приложения-банкомата...");

            System.Console.ReadKey();
        }



        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}