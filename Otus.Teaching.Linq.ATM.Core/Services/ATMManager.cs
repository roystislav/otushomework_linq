﻿using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }


        public User ShowUserInfo(string login, string password)
        {
            var query = Users.Where(x => x.Login.Equals(login) && x.Password.Equals(password));
            var result = query.ToList();
            if (result.Any())
            {
                return result.FirstOrDefault(); 
            }

            throw new System.Exception("Введены неверные данные!");

        }

        public Account ShowAccountInfo(User user)
        {

            var query = Accounts.Where(x => x.UserId.Equals(user.Id));

            var result = query.ToList();
            return result.FirstOrDefault();

        }

        public void ShowAccountHistoryInfo(User user)
        {

            var query = Accounts
                .Join(History,
                        x => x.Id,
                        y => y.AccountId,
                        (x, y) => new
                        {
                            AccountId = x,
                            HistoryId = y
                        })
                .Where(y => y.AccountId.UserId.Equals(user.Id));
            var result = query.ToList();
            
            if (result.Any())
            {
                foreach (var q in result)
                {
                    var oper = q.HistoryId.OperationType == OperationType.InputCash ? "Снятие" : "Пополнение";
                    System.Console.WriteLine($"Номер счета {q.AccountId.Id} {q.HistoryId.OperationDate} {oper} {q.HistoryId.CashSum}");
                }
            }
            else
            {
                throw new System.Exception("Отсутствуют данные по истории!");
            }
        }

        public void ShowInputCashOperations()
        {
            var query = from a in Accounts
                        join h in History
                        on a.Id equals h.AccountId
                        join u in Users
                        on a.UserId equals u.Id
                        where h.OperationType == OperationType.InputCash
                        orderby a.Id ascending
                        select new
                        {
                            Users = u,
                            History = h,
                        };

            var result = query.ToList();

            foreach (var q in result)
            {
                System.Console.WriteLine($"Номер операции   {q.History.Id}\n" +
                    $"Дата операции   {q.History.OperationDate}\n" +
                    $"Тип операции   {q.History.OperationType}\n" +
                    $"Сумма   {q.History.CashSum}\n" +
                    $"Логин   {q.Users.Login}\n" +
                    $"Фамилия   {q.Users.SurName}\n" +
                    $"Имя   {q.Users.MiddleName}\n");

            }
        }

        public void ShowCashMoreThanN(int Sum)
        {
            var query = Users
                .Join(Accounts,
                    x => x.Id,
                    y => y.UserId,
                    (x, y) => new
                    {
                        UserId = x,
                        AccountId = y
                    })
                .Where(c => c.AccountId.CashAll > Sum);

            var result = query.ToList();

            foreach (var q in result)
            {
                System.Console.WriteLine($"Номер счета   {q.AccountId.Id}\n" +
                   $"Дата операции   {q.AccountId.OpeningDate}\n" +
                   $"Сумма на счете   {q.AccountId.CashAll}\n" +
                   $"Логин   {q.UserId.Login}\n" +
                   $"Фамилия   {q.UserId.SurName}\n" +
                   $"Имя   {q.UserId.MiddleName}\n");
            }
        }


    }
}